ARG NODE_VERSION=14-alpine
FROM node:$NODE_VERSION as build

WORKDIR /code

# Copy and install dependencies
COPY package.json /code
COPY package-lock.json /code
RUN npm install

# Install and Update project to use Angular specific version according to the Build Arg
ARG ANGULAR_VERSION=12.2.4
RUN npm install -g @angular/cli@$ANGULAR_VERSION
RUN ng update @angular/core@$ANGULAR_VERSION @angular/cli@$ANGULAR_VERSION

# Build Angular project
COPY . /code
RUN npm run build

ARG NGINX_VERSION=1.20.1-alpine
FROM nginx:$NGINX_VERSION

COPY --from=build /code/dist/angular-sample-app /usr/share/nginx/html

EXPOSE 80